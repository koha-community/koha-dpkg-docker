# koha-dpkg-docker - generate Koha packages inside a docker container

This project provides a way to generate Debian (.deb) packages for the
Koha project, without the need to have any complicated setups.

This is specially useful for automated builds in CI/CD workflows.

## Usage

In order to generate Debian (.deb) packages for the Koha project you need to have

* a git clone of the Koha source (_/path/to/your/clone_ in the sample commands)
* a valid target directory (_/dest/path/for/debs_, it needs to exist and have the right permissions)

Environment variables:

* _VERSION_ will be used for the advertised Koha on version on the package.
* _RELEASE_ will be used to determine which version of Koha from https://debian.koha-community.org/koha/dists/ will be used for apt.
* _DEB_BUILD_OPTIONS_ may be set to `nocheck` in order to skip unit tests during package builds. This is recommended.

Once you have that, you need to checkout the branch you want to build packages :

```shell
export KOHACLONE=/path/to/your/clone
export DEBS_DIR=/dest/path/for/debs
cd $KOHACLONE
git fetch ; git checkout main -b my_main_build
# The clone needs to be clean!
git clean -d -x -f
```

Last step, build the packages:

```shell
docker run \
  --volume=$KOHACLONE:/koha \
  --volume=$DEBS_DIR:/debs \
  --privileged \
  --env VERSION="23.11.03~3" \
  --env RELEASE="23.11" \
  --env DEB_BUILD_OPTIONS="nocheck" \
  koha/koha-dpkg:main
```

Notice we used koha/koha-dpkg:__main__. You might need to ask on channel if a specific version of this tool
needs to be selected (specially for older versions). _main_ should be good enough nowadays.

You will find the built packages in the __debs__ dir (__/dest/path/for/debs__ in this example).

## Building the Docker images

If you want to build the images yourself, you can do it like this:

```shell
  $ docker build . -t koha/koha-dpkg:main
```

### Using pre-made base.tgz files

By default, the process builds a new base.tgz each time you run the tool. If you need to debug the
build process, you can build your own and have it used instead.

You can also prefer to keep your own _base.tgz_ If you need to save time, bandwidth, etc
and **you know what you are doing**. You can create your own image like this:

The base images are created using the following commands:

```shell
sudo pbuilder create --distribution bullseye --basetgz base.tgz
sudo pbuilder login  --save-after-login      --basetgz base.tgz
```

Inside the _pbuilder_ context, several things need to be added for things to work. Look at the
_pbuilder.sh_ file to get an insight on what you need to do on your own _base.tgz_.

Once you built it, you can change the command you use for running the tool so it uses
the your _base.tgz_ instead of building a new one:

```shell
docker run \
  --volume=$KOHACLONE:/koha \
  --volume=$DEBS_DIR:/debs \
  --volume=/var/cache/pbuilder/base.tgz:/var/cache/pbuilder/base.tgz \
  --privileged \
  --env VERSION="23.11.03~3" \
  --env RELEASE="23.11" \
  --env DEB_BUILD_OPTIONS="nocheck" \
  koha/koha-dpkg:main
```

## Troubleshooting

### unrepresentable changes to source
The Koha script that builds the packages doesn't accept the checkout to contain any file or dir that is not committed. Including files listed in `.gitignore`.

Example of directories that must be removed permanently or backed up elsewhere.

- node_modules
- koha-tmpl/intranet-tmpl/prog/css/maps/
- koha-tmpl/opac-tmpl/bootstrap/css/maps/
- .swp files
